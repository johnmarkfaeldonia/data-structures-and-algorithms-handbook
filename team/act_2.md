<!-- START bsis_2_visaya_christian -->

# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END bsis_2_visaya_christian -->

<!-- START act_2_anito_marjorie -->

# Aspiring Web Developer

## Marjorie Anito

👋 Aspiring Web Developer — 💌 marjorieanito@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_marjorie_anito.jpg](images/act_2_marjorie_anito.jpg)

### Bio

**Good to know:** Idc.

**Motto:** Roblox is layp..

**Languages:** C#, Javascript, C++, PHP

**Other Technologies:** Facebook, Instagram, Discord

**Personality Type:** [Turbulent Adventurer (ISFP-T)](https://www.16personalities.com/profiles/3b5e4b840a951)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_anito_marjorie -->

<!-- START act_2_basila_ellain -->

# Aspring Graphic Designer 

## Ellain Basila

👋 Aspiring Graphic Designer — 💌 ellainbasila@student.laverdad.edu.ph — Apalit, Pampanga

![alt act2_ellainbasila.jpg](images/act2_ellainbasila.jpg)

### Bio

**Good to know:** Howesoever i am active in my function inside the Church,

**Motto:** With God nothing shall be impossible.

**Languages:** Python

**Other Technologies:** ..

**Personality Type:** [Turbulent Advocate (INFJ-T)](https://www.16personalities.com/profiles/66b863f350609)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act2_ellainbasila -->

<!-- START act_2_belen_jireh -->

# Aspiring Cybersecurity Professional

## Jireh Belen

👋 Aspiring Cybersecurity Professional — 💌 jirehbelen@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_belen_jireh.jpg](images/act_2_belen_jireh.jpg)

### Bio

**Good to know:** I cant express my emotions and what I want to say most of the time. Sometimes I want to be alone and avoid talking to other people.

**Motto:** We do not remember days, we remember moments.

**Languages:** Javascript, C#, Python, CSS, HTML

**Other Technologies:** MySql, VSCode, Sublime Text, Visual Studio

**Personality Type:** [Turbulent Adventurer (ISFP-T))](https://www.16personalities.com/profiles/167b370e9d06d)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_belen_jireh -->

<!-- START act_2_bigtas_kurtd_daniel -->

# Aspiring Cybersecurity Professional

## Kurtd Daniel Bigtas

👋 Aspiring Cybersecurity Professional — 💌 kurtddanielbigtas@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_bigtas_kurtd_daniel.jpg](images/act_2_bigtas_kurtd_daniel.jpg)

### Bio

**Good to know:** I'm a very patient person. I can wait if there's a need. Willing to wait. I hate goodbyes.

**Motto:** In order to be safe, you need to know where you're vulnerable.

**Languages:** Java, C#, Python, CSS, HTML

**Other Technologies:** Kali Linux

**Personality Type:** [Protagonist (ENFJ-T)](https://www.16personalities.com/profiles/c28f51c5fdaac)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_bigtas_kurtd_daniel -->

<!-- START act_2_deguzman_sherline -->

# Aspiring Web Developer

## Sherline De Guzman

👋 Aspiring Web Developer — 💌 sherlinedeguzman@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_deguzman_sherline](images/act_2_deguzman_sherline.jpg)

### Bio

**Good to know:** Hi! My name is Sherline De Guzman I'm 19, years old from Pangasinan but im currenntly living in sampaloc, apalit Pampanga. I describe my self as hyper person.

**Motto:** I can do all things throught Crist who strengthens me.

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Turbulent Protagonist (ENFJ-T)](https://www.16personalities.com/profiles/6307806d65d1e)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act2_deguzman_sherline_ -->

<!-- START act_2_delacruz_jheriemia -->

# Aspiring Graphic Designer

## Jherie Mia S. Dela Cruz

👋 Aspiring Graphic Designer — 💌 jheriemiadelacruz@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_delacruz_jheriemia.jpg](images/act_2_delacruz_jheriemia.jpg)

### Bio

**Good to know:** eywan ko din:>

**Motto:** Bungee Gum has the properties of both rubber and gum.

**Languages:** Java, C#, HTML, CSS

**Other Technologies:** ...

**Personality Type:** [ Turbulent Virtuoso (ISTP-T)](https://www.16personalities.com/profiles/022c20cec65a5)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_delacruz_jheriemia -->

<!-- START act_2_dela_rama_john_ray_ben -->

# Aspiring Full Stack Developer

# John Ray Ben Dela Rama

👋 Aspiring Full Stack Developer — 💌 johnraybendelarama@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_dela_rama_john_ray_ben.jpg](images/act_2_dela_rama_john_ray_ben.jpg)

### Bio

**Good to know:** Idk...

**Motto:** Progress, not perfection.

**Languages:** Java, C#, C++, PHP, Js, HTML, CSS

**Other Technologies:** NodeJs, ExpressJs, MySql, MongoDB

**Personality Type:** [Mediator (INFP-T)](https://www.16personalities.com/profiles/c29aea438757c)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_dela_rama_john_ray_ben -->

<!-- START act_2_diga_johncarlo -->

# Aspiring Web Designer

## John Carlo Diga

👋 Aspiring Web Designer — 💌 johncarlodiga@student.laverdad.edu.ph — Santa Cruz, Laguna

![alt act_2_diga_johncarlo.jpg](images/act_2_diga_johncarlo.jpg)

### Bio

**Good to know:** I'm a fan of Designing. Sheesh.

**Motto:** Train your mind to see the good in every situation.

**Languages:** Java and C#

**Other Technologies:** VS Code, Visual Studio, NodeJS.

**Personality Type:** [Assertive Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/870b0a0d79321)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_diga_johncarlo -->

<!-- START act_2_johnmark_faeldonia -->

# Aspiring Secret

# Johnmark Faeldonia

👋 Aspiring Secret — 💌 johnmarkfaeldonia@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_faeldonia_johnmark.jpg](images/act_2_faeldonia_johnmark.jpg)

### Bio

**Good to know:**
Friendly

**Motto:** Less Women less problem

**Languages:** c#

**Other Technologies:** NodeJs,MySql

**Personality Type:** [Assertive Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/a47b00601202a)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_faeldonia_johnmark -->

<!-- START act_2_fualo_rachelle -->

# Aspiring Web Designer

## Rachelle Fualo

👋 Aspiring Web Designer — 💌 rachellefualo@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_rachelle.jpg](images/act_2_rachelle.jpg)

### Bio

**Good to know:** I am a dependable person and eager to learn new skills. I love to laugh even in the serious situation.

**Motto:** DREAMS don't work unless you DO!

**Languages:** CSS, HTML, C#, Javascript

**Other Technologies:** Visual Studio Code

**Personality Type:** [Turbulent Protagonist(ENFJ-T)](https://www.16personalities.com/enfj-personality)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_fualo_rachelle -->

<!-- START act_2_ibale_jayvee_brian -->

# Aspiring Cyber Security <!-- the one you want to be -->

## Jayvee Brian Ibale

👋 Aspiring Cyber Security — 💌 jayveebrianibale@student.laverdad.edu.ph — Pasong Tamo, Quezon City

![alt act_2_ibale_jayvee_brian.jpg](images/act_2_ibale_jayvee_brian.jpg)

**Good to know:** I am optimistic and friendly person. I want to learn more and explore more the IT WORLD.

**Motto:** If you promise certain thing, you must to fulfill that

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Entertainer (ESFP-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_ibale_jayvee_brian-->

<!-- START act_2_Vera_Jane_Lazaraga -->

# Graphic Designer

## Vera Jane Lazaraga

👋 Graphic Designer — 💌 verajanelazaraga@astudent.laverdad.edu.ph — Apalit, Pampanga

![alt act-2-vera-lazaraga.jpg](images/act-2-vera-lazaraga.jpg)

### Bio

**Good to know:** I am a type of person who is friendly to all of people.

**Motto:** Make memories you know you'll never forget.

**Languages:** HTML, CSS

**Other Technologies:** Photoshop

**Personality Type:** [Turbulent (ESFP-T)](https://www.16personalities.com/profiles/74882d01416fd)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_Vera_Jane_Lazaraga -->

<!-- START act_2_Allen_Magadia -->

# Aspiring Developer

## Allen Magadia

👋 Aspiring Developer — 💌 allenaebrammagadia@astudent.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_allen_magadia.jpg](images/act_2_allen_magadia.jpg)

### Bio

**Good to know:** I am a type of person who is a bit quiet and reserved. I am an introverted type of person and I prefer to think a lot about different things rather than talking.

**Motto:** The world is full of obvious things which nobody by any chance ever observes.

**Languages:** HTML, CSS, C# ,Javascript

**Other Technologies:** Photoshop, Davinci Resolve, After Effects, Unity

**Personality Type:** [Mediator (INFP-A)](https://www.16personalities.com/profiles/59d29bb82543a)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_Allen_Magadia -->

<!-- START act2_majait_rolando-->

# Web Designer/cyber Security

## Rolando M. Majait

👋 Web Designer/cyber Security— 💌rolandomajait@student.laverdad.edu.ph — Apalit, Pampanga

![alt act2_majait_rolando.jpg](images/act2_majait_rolando.jpg)

### Bio

**Good to know:** I'am a smilley and observant person. observe your personality.

**Motto:** don't promise anything, surprise is better than promises.
**Languages:** Html, Java, C#, CSS, Java, JavaScript, Python, SQL, PHP, PowerShell, and C

**Other Technologies:** VS Code, Mysql, pop

**Personality Type:**[Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/11728d2b9ea4e)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/profiles/11728d2b9ea4e)_

<!-- END act2_majait_rolando -->

<!-- START act_2_mondares_criselda -->

# Aspiring Cybersecurity Agent

## Criselda Mondares

👋 Aspiring Cybersecurity Agent — 💌 criseldamondares@student.laverdad.edu.ph — Silang Cavite

![alt act_2_mondares_criselda.jpg](images/act_2_mondares_criselda.jpg)

### Bio

**Good to know:** I'm a fan of cybersecrimes documentation, and I have something inside my heart where I want someday to put an end on unending crimes online.

**Motto:** stay lowkey and make them wonder.

**Languages:** Java,C#, and a bit of python

**Other Technologies:**Unity,VS Code, Xampp, Visual Studio, Visual Studio Code.

**Personality Type:** [Advocate (INFJ-T))](https://www.16personalities.com/profile)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_mondares_criselda -->

<!-- START act_2_nava_johnryk -->

# Aspiring Front-end web developer

## JohnRyk Nava

👋 Aspiring Front-end web developer — 💌 johnryknava@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_nava_johnryk.jpg](images/act_2_nava_johnryk.jpg)

### Bio

**Good to know:** I like quiet environment in work or off work. I preffered to be in an isolated are rather to be in a crowded.I always think positively even in hard times. I am organize person. I believe that for able to be achieve success, you must work hard first.

**Motto:** Never surrender.

**Languages:** Java, C#, CSS, HTML

**Other Technologies:** Microsft word, VS code

**Personality Type:** [Logistician (IST J-A)](https://www.16personalities.com/profiles/4d670cf3de316)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_nava_johnryk -->

<!-- START act_2_ortiz_anna -->

# Aspiring Game Developer <!-- the one you want to be -->

## Anna Ortiz

👋 Aspiring Game Developer — 💌 annaortiz@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_ortiz_anna.jpg](images/act_2_ortiz_anna.jpg)

### Bio

**Good to know:** I may look like I'm tired, and I am. I'm just having a hard time adjusting to face-to-face classes and adjusting to the climate here in the Philippines, having been staying at Qatar my whole life. This is normal for me. i like cookie game :D

**Motto:** Tomorrow is another day.

**Languages:** Java, C#, HTML

**Other Technologies:** VS Code, Unity :D

**Personality Type:** [Logician (INTP-T)](https://www.16personalities.com/profiles/42d31f40e3b73)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_ortiz_anna -->

<!-- START act_2_pagente_evelyn -->

# Aspiring Web Designer

## Evelyn Pagente

👋 Aspiring Web Designer — 💌 evelynpagente@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_pagente_evelyn.jpg](images/act_2_pagente_evelyn.jpg)

### Bio

**Good to know:** I am not friendly but I'm easy to make friends with.

**Motto:** Do what makes you happy coz life is short.

**Languages:** HTML, Css, C#, Java

**Other Technologies:** Visual Studio Code

**Personality Type:** [Protagonist Personality (ENFJ-A / ENFJ-T)](https://www.16personalities.com/enfj-personality)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_pagente_evelyn -->

<!-- START act_2_patiño_vhaugnn_diane -->

# Aspiring Game Developer

## Vhaugnn Diane Patiño

👋 Aspiring Game Developer — 💌 vhaugnndianepatino@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_patiño_vhaugnn_diane.jpg](images/act_2_patiño_vhaugnn_diane.jpg)

### Bio

**Good to know:** I'm excited to learn new things that pique my interest. I enjoy painting, playing the ukulele, and skateboarding, but I'm not particularly good at any of them.

**Motto:** Mottolog

**Languages:** Java, C#

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-T)](https://www.16personalities.com/profile)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_patiño_vhaugnn_diane -->

<!-- START act2_pili_rizabel_anne -->

# Aspiring Web Designer

## Rizabel Anne Pili

👋 Aspiring Web Designer — 💌rizabelannepili@student.laverdad.edu.ph Apalit, Pampanga

![alt act2_pili_rizabel_anne.jpg](images/act2_pili_rizabel_anne.JPG)

### Bio

**Good to know:** I like to be a web designer because art is one of my hobbies. I use to draw in my laptop with unlimited tools.

**Motto:** Time is Gold

**Languages:** Java, C# , CSS, HTML, soon Javascript and PHP

**Other Technologies:** Visual Studio Code only

**Personality Type:** [Mediator (INFP-T)](https://www.16personalities.com/profiles/4a123fa065a6d)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act2_pili_rizabel_anne -->

<!-- START act_2_rayos_peter_sthanlie -->

# Aspiring Cyber security professional

## Peter Sthanlie Rayos

👋 Aspiring Cyber Security Professional — 💌 petersthanlierayos@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_rayos_peter_sthanlie.jpg](images/act_2_rayos_peter_sthanlie.jpg)

### Bio

**Good to know:** I have ADHD, willing to learn, minimalist

**Motto:** Don't let the day ends without learning something new

**Languages:** Java, C#, HTML, CSS

**Other Technologies:** Pop OS Linux

**Personality Type:** [Turbulent Mediator (INFP-T)](https://www.16personalities.com/profiles/4414be7ad137a)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act_2_rayos_peter_sthanlie -->

<!-- START act2_razon_romel -->

# Game

## Romel A Razon

👋 Game Programmer — 💌 — romelrazon@student.laverdad.edu.ph-Calumpit,Bulacan

![alt act_2_razon_romel.jpg](images/act_2_razon_romel.jpg)

### Bio

**Good to know:** I want to introduce myself to all of you, what I always do when I have spare time, I play chess I enjoy it.

**Motto:** I can work under pressure

**Languages:** c#, java, markup language

**Other Technologies:** Microsoft word , power point , visual studio

**Personality Type:** [Turbulent Mediator (INFP-T)](https://www.16personalities.com/profiles/a9b9e3522f097)

\_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)

<!-- END act2_razon_romel -->

<!-- START act2_alexandria_nicole -->

# Web Designer

## Alexandria Nicole Tulipat

👋 Web Designer/Game Producer — 💌 alexandrianicoletulipat@student.laverdad.edu.ph — Apalit, Pampanga

![alt act2_tulipat_alexandria_nicole.jpg](images/act2_tulipat_alexandria_nicole.jpg)

### Bio

**Good to know:** I'm a straight-forward person. If I don't like you then I don't like you, periodt.

**Motto:** Mind over feelings; Think before you speak and know your actions.

**Languages:** Html, Java, C#

**Other Technologies:** Visual Studio, VS Code, MySql

**Personality Type:** [Protagonist (INTP-T)](https://www.16personalities.com/profiles/11728d2b9ea4e)

_Know your personality type, traits, role, and strategy at [16Personalities.com](https://www.16personalities.com/)_

<!-- END act2_alexandria_nicole -->
